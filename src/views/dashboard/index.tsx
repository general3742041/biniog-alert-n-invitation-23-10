import { FC } from "react"
import { useDashboard } from "./useDashboard.hook"

export interface DashboardViewProps {

}

export const DashboardView: FC<DashboardViewProps> = (props: DashboardViewProps) => {

    const { } = useDashboard({})

    return (
        <>
            This is home page and dashboard replica.
        </>
    )
}