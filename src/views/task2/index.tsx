import { FC, useState } from "react"
import { useTask2 } from "./useTask2.hook"
import HeaderWithComponent from "features/heading/header-with-components"
import { DefaultHeader } from "features/headers/default.headers";
import { InviteUser } from "features/auth/inviteUser";
import { AccountList } from "features/auth/AccountList";

export interface Task2ViewProps {

}

export const Task2View: FC<Task2ViewProps> = (props: Task2ViewProps) => {

    const { nav_items, active, setActive } = useTask2({})

    const SwitchView = () => {
        if (active == 'Accounts') return <AccountList/>
        if (active == 'Invite') return <InviteUser/>
        if (active == 'Invite Pending') return <AccountList/>
        else return <>Switch to your panel.</>
    }

    return (
        <>
            <HeaderWithComponent h='Task 2'/>
            <DefaultHeader appearance="subtle" justified nav_items={nav_items} active={active} />
            <SwitchView/>
        </>
    )
}