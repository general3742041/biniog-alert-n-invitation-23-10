import { FC, useState } from "react"
import { useTask1 } from "./useTask1.hook"
import { Button, ButtonGroup, Modal } from "rsuite"
import AlertModal from "components/modal/alertModal";
import HeaderWithComponent from "features/heading/header-with-components";

export interface Task1ViewProps {

}

export const Task1View: FC<Task1ViewProps> = (props: Task1ViewProps) => {

    const { } = useTask1({})

    const [modalTitle, setModalTitle] = useState<string>('')
    const [modalType, setModalType] = useState<'SUCCESS' | 'WARNING' | 'ERROR'>('SUCCESS')
    const [modalDescription, setModalDescription] = useState<string>('')
    const [open, setOpen] = useState(false);

    const openModal = (title: string, type: 'SUCCESS' | 'WARNING' | 'ERROR', description: string) => {
        setModalTitle(title);
        setModalType(type);
        setModalDescription(description);
        setOpen(true);
    }

    const data = {
        error:{
            title: 'A Error Title',
            description: 'A Error message description.'
        },
        success:{
            title: 'A Success Title',
            description: 'A Success message description.'
        },
        warning:{
            title: 'A Warning Title',
            description: 'A Warning message description.'
        }
    }

    return (
        <>
            <HeaderWithComponent h='Task 1'/>
            <hr />
            <ButtonGroup style={{ marginTop: 12 }} justified>
                <Button appearance='ghost' color="green" onClick={() => { openModal(data.success.title, 'SUCCESS', data.success.description) }}>Success</Button>
                <Button appearance='ghost' color="yellow" onClick={() => { openModal(data.warning.title, 'WARNING', data.success.description) }}>Warning</Button>
                <Button appearance='ghost' color="red" onClick={() => { openModal(data.error.title, 'ERROR', data.success.description) }}>Error</Button>
            </ButtonGroup>
            <AlertModal title={modalTitle} description={modalDescription} type={modalType} open={open} setOpen={setOpen} />
        </>
    )
}
