import { RouterProvider } from 'react-router-dom';
import './App.css';
import { router } from 'utils/router';
import withDefaultLayout from 'layouts/withDefault.layout.hoc';

function App() {
  return (
    <>
      <RouterProvider router={router} />
    </>
  );
}

export default withDefaultLayout(App);
