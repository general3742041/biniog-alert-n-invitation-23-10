import { createRef, FC, useContext, useEffect, useRef, useState } from "react"
import { Button, IconButton, Nav, PickerHandle, SelectPicker, Sidenav } from "rsuite"

import SearchIcon from '@rsuite/icons/Search';
import CloseIcon from '@rsuite/icons/Close';
import AppSelectIcon from '@rsuite/icons/AppSelect';
import TagDateIcon from '@rsuite/icons/TagDate';
import CouponIcon from '@rsuite/icons/Coupon';
import SpeakerIcon from '@rsuite/icons/Speaker';
import DeviceOtherIcon from '@rsuite/icons/DeviceOther';
import ScatterIcon from '@rsuite/icons/Scatter';
import TmallIcon from '@rsuite/icons/Tmall';
import PeoplesCostomizeIcon from '@rsuite/icons/PeoplesCostomize';

import DashboardIcon from '@rsuite/icons/legacy/Dashboard';
import MagicIcon from '@rsuite/icons/legacy/Magic';

import "./style.scss"
import { SuperContext } from "utils/contexts";
import { useLocation } from "react-router-dom";

export interface ConstSideBarProps {
}

export const ConstSideBar: FC<ConstSideBarProps> = (props: ConstSideBarProps) => {

    const superContext = useContext(SuperContext);
    const [path, setPath] = useState<string>(window.location.pathname)
    useEffect(() => {
        setPath(window.location.pathname)
    }, [])

    return (
        <>
            <Sidenav className="const-sidenav" expanded={superContext.screenWidth > 720}>
                <Sidenav.Header id='clickbox' >
                    {superContext.screenWidth > 720 ?
                        <h2 style={{ height: '35px', margin: '10px', color: 'red' }}>Biniyog</h2> :
                        <h2 style={{ height: '35px', margin: '10px', color: 'red' }}>B</h2>
                    }
                </Sidenav.Header>
                <hr />

                <Sidenav.Body>
                    <Nav>
                        <Nav.Item eventKey="1" icon={<SpeakerIcon />} href="/task1" active={path == "/task1"}>
                            Task 1
                        </Nav.Item>
                        <Nav.Item eventKey="2" icon={<CouponIcon />} href="/task2" active={path == "/task2"}>
                            Task 2
                        </Nav.Item>
                    </Nav>
                </Sidenav.Body>
            </Sidenav>
        </>
    )
}