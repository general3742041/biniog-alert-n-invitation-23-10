import rest_api from 'app/rest-api';

export const AuthApi = rest_api.injectEndpoints({
    overrideExisting: true,
    endpoints: (build) => ({
        inviteUser: build.mutation({
            query: (data: { uuid: string, payload: any }) => ({ method: "POST", url: 'security/accounts/' + data.uuid + '/invite-user', data: data.payload })
        }),


    })
})


export const {
    useInviteUserMutation,

} = AuthApi
