import { AccountInviteTab } from 'models/accounts';
import { useState } from 'react';
import { NavItemProps } from 'rsuite';

export interface UseAccountListProps {

}

export interface UseAccountListInterface {
    nav_items: NavItemProps[]
    active: AccountInviteTab
    setActive: (value: AccountInviteTab) => void
}

export const useAccountList = (props: UseAccountListProps): UseAccountListInterface => {

    const [active, setActive] = useState<AccountInviteTab>('Accounts')
    const nav_items: NavItemProps[] = [
        {
            children: 'Accounts',
            onSelect: (eventKey?: string, e?: any) => { setActive('Accounts') }
        },
        {
            children: 'Invite',
            onSelect: (eventKey?: string, e?: any) => { setActive('Invite') }
        },
        {
            children: 'Invite Pending',
            onSelect: (eventKey?: string, e?: any) => { setActive('Invite Pending') }
        }
    ]

    return {
        nav_items,
        active,
        setActive,
    }
}