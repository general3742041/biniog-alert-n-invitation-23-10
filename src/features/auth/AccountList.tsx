import { FC, useState } from "react"
import { useAccountList } from "./hooks/useAccountList.hook";
import { Input, InputGroup, List } from "rsuite";
import SearchIcon from '@rsuite/icons/Search';

export interface AccountListProps {

}

export const AccountList: FC<AccountListProps> = (props: AccountListProps) => {

    const { nav_items, active, setActive } = useAccountList({})

    const styles = {
        align: 'center',
        marginTop: '30px',
        padding: '20px',
    };

    return (
        <div style={styles}>
            <i>Task pending . . .</i>
            <List bordered>
                <List.Item>Roses are red</List.Item>
                <List.Item>Violets are blue</List.Item>
                <List.Item>Sugar is sweet</List.Item>
                <List.Item>And so are you</List.Item>
            </List>
        </div>
    )
}