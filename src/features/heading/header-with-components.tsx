import DefaultHeading from 'components/heading/defaultHeading'
import { DefaultHeadingProps } from 'components/heading/defaultHeading';
import Buttons from 'components/button/buttons';
import { DefaultButtonProps } from 'components/button/defaultButton';

import './style.scss'
import { SelectPicker, SelectPickerProps } from 'rsuite';

export interface HeaderWithComponentProps extends DefaultHeadingProps {
    buttons?: DefaultButtonProps[];
    selectPicker?: SelectPickerProps<any>;
}

const HeaderWithComponent = (props: HeaderWithComponentProps) => {

    const { h, s = '5', c, buttons, selectPicker } = props

    return (
        <div className="header-with-component">
            <table>
                <tbody>
                    <tr>
                        <td className='header-text'><DefaultHeading h={h} s={s} c={c} /></td>
                        {buttons && <td className='header-button'><Buttons buttons={buttons} /></td>}
                        {selectPicker && <td className='header-select-picker'><SelectPicker {...selectPicker} /></td>}
                    </tr>
                </tbody>
            </table>
        </div>
    )
}
export default HeaderWithComponent