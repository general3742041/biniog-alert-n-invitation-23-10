import { FC } from "react"
import { DashboardView } from "views/dashboard"

export interface DashboardPageProps {

}

export const DashboardPage: FC<DashboardPageProps> = (props: DashboardPageProps) => {
    return (
        <>
            <DashboardView/>
        </>
    )
}