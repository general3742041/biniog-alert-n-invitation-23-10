import { FC } from "react"
import { Task2View } from "views/task2"

export interface Task2PageProps {

}

export const Task2Page: FC<Task2PageProps> = (props: Task2PageProps) => {
    return (
        <>
            <Task2View/>
        </>
    )
}