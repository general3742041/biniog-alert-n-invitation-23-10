import DefaultButton, { DefaultButtonProps } from "./defaultButton"


export interface ButtonsProps {
    buttons: DefaultButtonProps[]
}

const Buttons = (props: ButtonsProps) => {
    const { buttons } = props
    return (<>
        {buttons.map((b: DefaultButtonProps, index: number) =>
            <DefaultButton {...b} key={index} />
        )}
    </>)
}

export default Buttons