import { createBrowserRouter } from "react-router-dom";

import { DashboardPage } from "pages/top-nav-dashboard";
import { Task1Page } from "pages/side-nav-task1";
import { Task2Page } from "pages/side-nav-task2";

export const router = createBrowserRouter([
    {
        path: "/",
        element: <DashboardPage />,
    },
    {
        path: "/task1",
        element: <Task1Page />,
    },
    {
        path: "/task2",
        element: <Task2Page />,
    },
]);